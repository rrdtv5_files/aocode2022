#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

vector<string> read_data_into_vector(){
	vector<string> result;
	string temp;
	ifstream file("calories.txt");
	while(getline(file,temp)){
		result.push_back(temp);
	}
	return result;
}

vector<int> take_sums_of_blocks(vector<string> input){
	vector<int> result;
	int sum_temp = 0;
	for(auto entry : input){
		if(entry != ""){
			sum_temp += stoi(entry);
		}
		else{
			result.push_back(sum_temp);
			sum_temp = 0;
			}
	}
	return result;
}

int main(){
	auto data = read_data_into_vector();
	vector<int> sums = take_sums_of_blocks(data);
	sort(sums.begin(), sums.end());
	int vector_length = sums.size();
	cout << "The desired three biggest sums of calories are:"<<endl;
	for(int i=vector_length-3; i<vector_length; i++){
		cout << sums[i]<<endl;
	}
	cout << "Program completed."<< endl;
	return 0;
}
