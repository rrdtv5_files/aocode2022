#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>

using namespace std;

vector<vector<int>> points_task1 = {{4,8,3},{1,5,9},{7,2,6}};
vector<vector<int>> points_task2 = {{3,4,8},{1,5,9},{2,6,7}};
map<string,int> opponent_dict={{"A",0},{"B",1},{"C",2}};
map<string,int> my_dict={{"X",0},{"Y",1},{"Z",2}};

vector<string> read_data_into_vector(){
	vector<string> result;
	string temp;
	ifstream file("strategy_guide.txt");
	while(getline(file,temp)){
		result.push_back(temp);
	}
	return result;
}

int check_single_result(string player_input, vector<vector<int>> result_matrix){
	stringstream player_input_stream(player_input);
	string substring;
	vector<string> input;
	while(getline(player_input_stream, substring, ' ')){
		input.push_back(substring);
	}
	string opponent_action = input[0];
	string my_action = input[1];
	return result_matrix[opponent_dict[opponent_action]][my_dict[my_action]];
}

vector<int> collect_results(vector<string> strategy_guide, vector<vector<int>> result_matrix){
	vector<int> result;
	for(string element : strategy_guide){
		int points_single_round = check_single_result(element, result_matrix);
		result.push_back(points_single_round);
	}
	return result;
}

int sum_up(vector<int> input){
	int sum=0;
	for(int element : input){
		sum += element;
	}
	return sum;
}

int main(){
	vector<int> all_game_rounds_task1 = collect_results(read_data_into_vector(), points_task1);
	vector<int> all_game_rounds_task2 = collect_results(read_data_into_vector(), points_task2);
	cout << "The total score for the first interpretation of the strategy guide is:" << sum_up(all_game_rounds_task1) << endl;
	cout << "The total score for the second interpretation of the strategy guide is:" << sum_up(all_game_rounds_task2) << endl;
	cout << "Program completed."<< endl;
	return 0;
}
