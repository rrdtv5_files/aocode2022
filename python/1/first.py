import numpy as np

#task 1.1
calories = []
with open("calories.txt") as file:
	for line in file:
		if line == "\n":
			calories.append("delimiter")
		else:
			calorie_number = float(line.replace("\n",".0"))
			calories.append(calorie_number)

def extract_first_sublist(complete_list):
	result = []
	block = []
	for item in complete_list:
		if item == "delimiter":
			result.append(block.copy())
			block =[]
		else:
			block.append(item)
	return result

calories_blocks = extract_first_sublist(calories)
calories_sum = [np.sum(block) for block in calories_blocks]
calories_max = np.max(calories_sum)
print(f"The maximum amount of calories carried by an elf is {calories_max}!")

# task 1.2
calories_sum_sorted = np.sort(calories_sum)
calories_max_3 = calories_sum_sorted[-3:]
print(f"The top three elves are carrying {str(calories_max_3)} calories respectively!")
print(f"That makes them carry {np.sum(calories_max_3)} calories together!")
