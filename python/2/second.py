import numpy as np

#task 2.1
strategy_guide =[]
with open("strategy_guide.txt") as file:
	for line in file:
		entry = line.split(" ")
		entry = [element.replace("\n","") for element in entry]
		strategy_guide.append(entry)

# ABC,XYZ as row,column indices
# matrix gives points I win for given player moves
result_table = np.array([[4,8,3],[1,5,9],[7,2,6]])
opponent_dict = {'A':0,'B':1,'C':2}
my_dict = {'X':0,'Y':1,'Z':2}

def check_single_result(player_input,result_matrix):
	opponent_action = player_input[0]
	my_action = player_input[1]
	result = result_matrix[opponent_dict[opponent_action],my_dict[my_action]]
	return result

def total_score(strategy_guide,result_matrix):
	all_results = []
	for player_input in strategy_guide:
		score_temp = check_single_result(player_input,result_matrix)
		all_results.append(score_temp)
	score = np.sum(all_results)
	return score

print(f"The total score for the given strategy guide is {total_score(strategy_guide,result_table)}!")

#task 2.2
result_table_adjusted = np.array([[3,4,8],[1,5,9],[2,6,7]])
print(f"The total score for the given strategy guide with adjusted interpretation is {total_score(strategy_guide,result_table_adjusted)}!")
